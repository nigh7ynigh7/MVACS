﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _010_Datetime
{
    class Program
    {
        static void Main(string[] args)
        {

            DateTime myVal = DateTime.Now;
            /*
                Console.WriteLine(myVal.ToString());
                Console.WriteLine(myVal.ToShortDateString()); //returns just the date
                //date presentation is based on the person's computer.
                Console.WriteLine(myVal.ToShortTimeString()); // just the time
                Console.WriteLine(myVal.ToShortDateString()); //with pm
                Console.WriteLine(myVal.ToLongDateString()); //full date
                Console.WriteLine(myVal.ToLongTimeString()); //adds seconds

                //date operations are possible
                //add to dates and stuff.

                Console.WriteLine(myVal.AddDays(3).ToLongDateString());
                Console.WriteLine(myVal.AddHours(3).ToLongTimeString());

                Console.WriteLine(myVal.AddDays(-3).ToLongDateString());
                Console.WriteLine(myVal.Month.ToString()); // month is 1 based and not 0 based.
                Console.WriteLine(myVal.DayOfWeek.ToString()); //wed
                Console.WriteLine(myVal.DayOfYear.ToString()); //up to 365 or 366

                // you can pas sin a date
                DateTime myBirthday = new DateTime(1994, 05, 19); // with date and stuff.
                Console.WriteLine(myBirthday.ToLongDateString()); // i was born on a thursday

                DateTime bday = DateTime.Parse("5/19/1994");

                TimeSpan myAge = DateTime.Now.Subtract(myBirthday); // for figuring out
                Console.WriteLine(myAge.TotalDays.ToString());
            */


            Console.ReadKey();
        }
    }
}
