﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _004Stoperators
{
    class Program
    {
        static void Main(string[] args)
        {
            //expressions = operators + operands
            //expressions is a line of code

            //statement = complete expressions composed of expressions
            //expressions are made of oeprands and operaotrs
            //operators are like verbs

            /*
                known operators
                +
                =
                ==
                () could change the meaning of the code
                . member access (accessor)
                    heavily used in OOP
            */

            
            //declarations statements
            int i = 1;
            //expressions statements
            i = 2 + i + 3;

            //decision statems
            //iteration statements
            Console.WriteLine(i);
            Console.ReadKey();
            
        }
    }
}
