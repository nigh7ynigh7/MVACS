﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _011_Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            //new instance uses the keyword new
            Car muhCar = new Car();
            //class != objection, duh
            //class = blueprint/pattern
            //object created from blueprint
            //many objects from same class, objects are distinct


            muhCar.Make = "BMO";
            muhCar.Model = "z812384y";
            muhCar.Year = 19999;
            muhCar.Color = "trnasparent";

            //assigning values to properties.
            //use it = get
            //change it = set

            Console.WriteLine(
                "{3} {0} {1}, {2}",
                muhCar.Make, 
                muhCar.Model, 
                muhCar.Year, 
                muhCar.Color);

            //code to work with get/set op are called getters and setters

            //double value = determineMarketValue(muhCar);
            double value = muhCar.determineMarketValue();
            Console.WriteLine("{0:C}", value);
            Console.ReadKey();
        }
        //use _ to mark input params
        private static double determineMarketValue(Car _car) {
            double carval = 100.0;

            //imagination!
            //magic!

            return carval;
        }
    }

    class Car {
        //properties are the characteristics of a class
        //they have something about them.
        //represent something the class can do.
        //property = attribute = characteristics
        public string Make { get; set; }
        //this way of making it is exclusive to c#
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        //this is some information of a car.

        public double determineMarketValue() {
            //this acceses homegrown values
            //values from itself (private
            double carval = 100.0;


            //use this to help id code from the class
            //good for differenciation
            if (this.Year > 1999)
                carval += 5000;
            else
                carval += 1000;

            return carval;
        }
    }
}
