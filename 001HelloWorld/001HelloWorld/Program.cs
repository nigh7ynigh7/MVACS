﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            //printf();
            Console.WriteLine("Hello World");
            //scanf();
            Console.ReadLine();

            /*
                clr
                a protective bubble to protect the user and simplify functions

                methods
                block of code

                class
                group related coded

                namespace
                another way to organize code

                Namespace>class>methods

                don't touch a .csproj xml file

                integrated debuging environment
                    see code being executed line by line

                in the debug folder (/bin/debug/)
                    comes with extra stuff

                change from debug to release (toolbar)
                    taskbar > build > build solution
                        ctrl + shift + b
                        files kind of stay the same
                        has a .manifest file
            */
        }
    }
}
