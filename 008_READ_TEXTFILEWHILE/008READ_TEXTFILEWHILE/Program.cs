﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008READ_TEXTFILEWHILE
{
    class Program
    {
        static void Main(string[] args)
        {
            //stream sucks in data from the document.
            //read it in any order you want.
            //for this to work, the Values.txt file must be copied to the build output folder.
            StreamReader myReader = new StreamReader("Values.txt");

            //you can:
            // System.IO.StreamReader myReader = new System.IO.StreamReader("Values.txt");

            string line = "";

            //indeterminate loops
            while (line != null) {
                line = myReader.ReadLine();
                if (line != null)
                    Console.WriteLine(line);
            }
            myReader.Close();

            //any file that is being called upon must be closed when you're done with them
            //if you don't close them, they will stay active....

            Console.ReadKey();
        }
    }
}

/*
    Using references files.
    Serves an alternative to System.XXX.YYY abc = new System.XXX.YYY();
*/
