﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_For_Iteration
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                //Console.WriteLine(i.ToString());
                if (i == 7)
                {
                    Console.WriteLine("Found 7!");
                    break;
                }
            }

            //keystroke is
            //for + tab*2
            for (int myVal = 0; myVal < 12; myVal++)
            {
                Console.WriteLine(myVal);
            }
            Console.ReadKey();
        }
    }
}
