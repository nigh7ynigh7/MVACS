﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _012_ObjectLifetime
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            //car is an reference of an object
            //object into the memory
            //you must maintain the handle
            //c# checks the reference to see if it's in use.
            //garbage collector

            Car other = car;
            //2 references to the same object, points to a physical point
            //a new handle
            //when code block is left, this handle is lost

            other.Year = 2001;
            Console.WriteLine("{0};{1}", other.Year, car.Year);

            //this dereferences the object
            other = null;
            Console.WriteLine("{0}", car.Year);
            car = null;
            //garbage collector works when it wants to
            //you gotta wait a little bit.

            //you can write code at the instanciation of an object
            //constructors let you do that

            Car car3 = new Car("Ford", "Escape", 2015, "Green");
            Car.myMethod();
            Console.ReadKey();
        }
    }
    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        public double OriginalPrice { get; set; }


        //you can overload constructors
        public Car() {
            this.Make = "Nissan";
        }

        public Car(string mae, string model, int year, string color) {
            Make = Make;
            Model = model;
            Year = year;
            Color = color;
        }

        public static void myMethod() { }

    }
}
//static methods are classed base
//you don't need to instanciate a class to use these methods/parameters