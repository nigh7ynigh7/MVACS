﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003Decisions
{
    class Program
    {
        static void Main(string[] args)
        {

            //Console.WriteLine("Type and press enter");
            //string usrval;
            //usrval = Console.ReadLine();
            //Console.WriteLine("You typed: " + usrval);




            Console.WriteLine("World you prefer dor 1 e or 3?");
            string usrval = Console.ReadLine();

            //string msg = "";

            //if (usrval == "1")
            //    msg = "You got a car";
            //else if (usrval == "2")
            //    msg = "New boat!";
            //else if (usrval == "3")
            //    msg = "New cat!";
            //else
            //    msg = "No Conditions, you luuse";

            //Console.WriteLine(msg);

            string msg = (usrval == "1") ? "boat" : "strand of lint"; // selector syntax
            //string msg = "black";
            //Console.WriteLine("You won {0}, {1}, {2}", msg, "ok?", Console.ReadLine());

            Console.WriteLine("You won {0}", msg);
            /*
                {0} first val
                {1} second val

                like %i  from c, java, and javascript
                type independent
            */


            Console.ReadLine();

            //good to make code clean before moving on
            //make it readable to the future you
        }
    }
}
