﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelperMethod
{
    class Program
    {
        // helper methods
        /*
            it's a method that helps another method complete its tasks.

        */
        static void Main(string[] args)
        {
            string myVal = superSecretFormula("buddy, i'm guy");
            Console.WriteLine(myVal);
            Console.ReadKey();
        }

        private static string superSecretFormula() {
            return "Hello Wold";
        }
        private static string superSecretFormula(string name) {
            return String.Format("Hello, {0}", name);
        }

        /*
            overloading is 2 methods, same name, different parameters (different datatypes)

            
        */
    }
}
