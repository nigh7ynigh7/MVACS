﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002Variables
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            int x;
            int y;

            x = 7;
            y = x + 3;
            Console.WriteLine(y);
            */

            //string myFirstName;
            //myFirstName = "Ba";

            //string myFirstName = "Ba";

            //var myFirstName = "Ba";

            //Console.WriteLine(myFirstName);

            int x = 7;
            //string y = "Ba";
            string y = "5";
            string myFirstType = x.ToString() + y;

            //int mySecond = x + y;

            int mySecondTry = x + int.Parse(y);

            Console.WriteLine(myFirstType);

            Console.ReadLine();

        }
    }
}
