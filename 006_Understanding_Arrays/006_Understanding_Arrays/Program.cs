﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_Understanding_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //arrays are []
            /*int[] numbers = new int[5];
            numbers[0] = 4;
            numbers[1] = 8;
            numbers[2] = 15;
            numbers[3] = 16;
            numbers[4] = 23;*/

            /*int[] numbers = new int[] { 4, 8, 15, 16, 23, 42 };
            Console.WriteLine(numbers[5].ToString());
            Console.ReadKey();*/

            /*string[] names = new string[] { "Eddie Morra", "Brian Finch", "Rebecca", "Boyl" };
            foreach (var name in names)
            {
                Console.WriteLine(name);
            }
            Console.ReadKey();*/

            string zig = "All you base are belong to us. Haha. " +
                "Make your time. All your base is Belong to us.";

            char[] charArray = zig.ToCharArray();

            Array.Reverse(charArray);

            foreach (var c in charArray)
            {
                Console.Write(c);
            }

            Console.ReadKey();
        }
    }
}
