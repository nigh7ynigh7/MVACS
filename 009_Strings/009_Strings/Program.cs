﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            //backslashes are special - they are escape characters
            //they help produce something else
            //string myString = "Go to your C:\\ drive";

            //escaped characers here would be the \"
            //backslash in use
            //string myString = "My \"so called\" life";

            //break line = \n
            //string myString = "what if i need a \n new line?";

            //String.Format subs in new thingys
            //string myString = string.Format("{0}!","Bonzai");
            //string myString = string.Format("Make {0} (Model:{1})", "BMW", "760LI");

            //:C = currency, locally based.
            //string myString = string.Format("{0:C}", 123.45);

            //:N = adds commas and decimals
            //string myString = string.Format("{0:N}", 123456789);

            //:P is percentage
            //string myString = string.Format("{0:P}",.123);

            //string myString = "";

            //#### works as placeholders
            //starts from right to left
            //myString = string.Format("Phone Number: {0:(###) ###-####}", 1234567890);
            //if there are more string/characers, then yeah, it would simply keep on adding to the firstgroup of #
            /*
                for (int i = 0; i < 100; i++)
                {
                    //myString = myString + "--" + i.ToString();
                    myString += "--" + i.ToString();
                }
            */
            //auto concatenation is easier.
            //there are classes that work with concats.

            //you will use StringBuilder
            /*
                StringBuilder myString = new StringBuilder();

                for (int i = 0; i < 100; i++)
                {
                    myString.Append("--");
                    myString.Append(i);
                }
            */
            //string builder makes the whole process faster.


            string myString = " That's what she said    ";

            //myString = myString.Substring(5, 14); // cuts a string
            //0 based


            //myString = myString.ToUpper(); // good for evaluation 

            //myString = myString.Replace(" ", "--"); //good for transfering information with a certain encoding to another

            myString = String.Format(
                "Length before: {0}, after: {1}",
                myString.Length,
                myString.Trim().Length); //trim removes all the leading and trailing whitespaces
                    //this 3rd parameter here is in an example of chaining.

            Console.WriteLine(myString);
            Console.ReadKey();
        }
    }
}
